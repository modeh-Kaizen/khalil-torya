package com.khalil.torya;

import android.os.Parcel;
import android.os.Parcelable;

public class Answer implements Parcelable {

    private String answerText;
    private String answerTag;

    public Answer(String answerText, String answerTag) {
        this.answerText = answerText;
        this.answerTag = answerTag;
    }

    protected Answer(Parcel in) {
        answerText = in.readString();
        answerTag = in.readString();
    }

    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel in) {
            return new Answer(in);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };

    public String getAnswerText() {
        return answerText;
    }

    public String getAnswerTag() {
        return answerTag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(answerText);
        parcel.writeString(answerTag);
    }
}
