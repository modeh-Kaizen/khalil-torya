package com.khalil.torya;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RadioGroup answersRadioGroup;
    private RadioButton answerA;
    private RadioButton answerB;
    private RadioButton answerC;
    private RadioButton answerD;
    private Button nextBtn;
    private int currentCount = 0;
    private TextView questionText;
    private ImageView questionImage;
    private String[] finalAnswers;
    ArrayList<String> trueAnswers = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // views binding
        questionText = (TextView) findViewById(R.id.question);
        questionImage = (ImageView) findViewById(R.id.sign);
        answersRadioGroup = (RadioGroup) findViewById(R.id.answers_radioGroup);
        answerA = (RadioButton) findViewById(R.id.answerA);
        answerB = (RadioButton) findViewById(R.id.answerB);
        answerC = (RadioButton) findViewById(R.id.answerC);
        answerD = (RadioButton) findViewById(R.id.answerD);
        nextBtn = (Button) findViewById(R.id.next_btn);


        // read data from files and return questions array
        final ArrayList<Question> questions = convertJsonToObject();
        final List<Answer> answers = questions.get(0).getAnswers();

        // initialization for the first question

        finalAnswers = new String[questions.size()];
        questionText.setText(questions.get(0).getTitle());
        finalAnswers[0] = "NA";

        Glide.with(this).load(questions.get(currentCount).getImagePath()).into(questionImage);
        setAnswers(answers);




        // next button action
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ++currentCount;
                if (currentCount == questions.size() - 1) {
                    nextBtn.setText("تأكيد!");
                }
                if (currentCount < questions.size()) {
                    answersRadioGroup.clearCheck();
                    questionImage.setImageDrawable(null);
                    //Log.d("answer", String.valueOf(finalAnswers[currentCount]));
                    questionText.setText(questions.get(currentCount).getTitle());
                    if (!questions.get(currentCount).getImagePath().equals("")) {
                        Glide.with(getApplicationContext()).load(questions.get(currentCount).getImagePath()).into(questionImage);
                    }

                    List<Answer> answers1 = questions.get(currentCount).getAnswers();
                    setAnswers(answers1);
                } else {
                    Intent i = new Intent(getApplicationContext(), ResultActivity.class);
                    i.putExtra("finalResult", finalAnswers);
                    i.putExtra("trueAnswers", trueAnswers);
                    i.putParcelableArrayListExtra("questions", questions);
                    startActivity(i);
                }
            }
        });




        // when we select a radio button this method will be triggered and we will find which radio button has been pressed.
        answersRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.answerA) {
                    finalAnswers[currentCount] = "A";
                } else if (checkedId == R.id.answerB) {
                    finalAnswers[currentCount] = "B";
                } else if (checkedId == R.id.answerC) {
                    finalAnswers[currentCount] = "C";
                } else if (checkedId == R.id.answerD) {
                    finalAnswers[currentCount] = "D";
                }
            }

        });


    }

    // get returned string from loadJSONFromAsset method and convert it to question object.
    public ArrayList<Question> convertJsonToObject() {
        ArrayList<Question> questionsArrayList = new ArrayList<Question>();
        Bundle extras = getIntent().getExtras();
        String fileName = "";
        if (extras != null) {
            fileName = extras.getString("exam");
        }

        String privateJson = loadJSONFromAsset(this, fileName);
        try {
            JSONObject obj = new JSONObject(privateJson);
            JSONArray questions = obj.getJSONArray("Questions");
            for (int i = 0; i < questions.length(); i++) {
                JSONObject question = questions.getJSONObject(i);
                String id = question.getString("ID");
                String title = question.getString("Title");
                String answerA = question.getString("AnswerA");
                String answerB = question.getString("AnswerB");
                String answerC = question.getString("AnswerC");
                String answerD = question.getString("AnswerD");
                String imagePath = question.getString("ImagePath");
                String trueAnswer = question.getString("TrueAnswer");
                List<Answer> answers = new ArrayList<Answer>();
                answers.add(new Answer(answerA, "A"));
                answers.add(new Answer(answerB, "B"));
                answers.add(new Answer(answerC, "C"));
                answers.add(new Answer(answerD, "D"));
                trueAnswers.add(trueAnswer);
                //questionsArrayList.add(new Question(id,title,imagePath,answers,answerA,answerB,answerC,answerD,trueAnswer));
                questionsArrayList.add(new Question(id, title, imagePath, answers, trueAnswer));

            }
            return questionsArrayList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    // load json from file and return string
    public String loadJSONFromAsset(Context context, String fileName) {
        String json = null;

        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    // set the data for the answers of the question.
    public void setAnswers(List<Answer> answers) {
        finalAnswers[currentCount] = "NA";
        answerA.setCompoundDrawables(null, null, null, null);
        answerB.setCompoundDrawables(null, null, null, null);
        answerC.setCompoundDrawables(null, null, null, null);
        answerD.setCompoundDrawables(null, null, null, null);
        String a = answers.get(0).getAnswerText();
        String b = answers.get(1).getAnswerText();
        String c = answers.get(2).getAnswerText();
        String d = answers.get(3).getAnswerText();
//        final Drawable[] aImg = new Drawable[1];
//        ImageView image = new ImageView(this);
        if (a.contains("https")) {
            setRadioButtonDrawable(answerA, this.getResources().getDrawable(R.drawable.qa));

        } else {
            answerA.setText(a);
        }

        if (a.contains("https")) {
            setRadioButtonDrawable(answerB, this.getResources().getDrawable(R.drawable.qb));

        } else {
            answerB.setText(b);
        }
        if (c.contains("https")) {
            setRadioButtonDrawable(answerC, this.getResources().getDrawable(R.drawable.qc));

        } else {
            answerC.setText(c);
        }
        if (d.contains("https")) {
            setRadioButtonDrawable(answerD, this.getResources().getDrawable(R.drawable.qd));

        } else {
            answerD.setText(d);
        }

    }

    // if the answer was an image so we need to set an image instead of text for the radio button.
    public void setRadioButtonDrawable(RadioButton rb, Drawable img) {
        rb.setText("");
        img.setBounds(0, 0, 200, 200);
        rb.setCompoundDrawables(img, null, null, null);
    }
}


