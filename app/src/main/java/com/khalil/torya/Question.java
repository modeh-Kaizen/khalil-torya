package com.khalil.torya;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Question implements Parcelable {

    private String id;
    private String title;
    private String imagePath;
    private List<Answer> answers;
    private String trueAnswer;


    public Question(String id, String title, String imagePath,List<Answer> answers, String trueAnswer) {
        this.id = id;
        this.title = title;
        this.imagePath = imagePath;
        this.trueAnswer = trueAnswer;
        this.answers = answers;
    }

    protected Question(Parcel in) {
        id = in.readString();
        title = in.readString();
        imagePath = in.readString();
        trueAnswer = in.readString();
        answers = in.readArrayList(Answer.class.getClassLoader());
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImagePath() {
        return imagePath;
    }


    public String getTrueAnswer() {
        return trueAnswer;
    }
    public List<Answer> getAnswers(){
        return answers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(imagePath);
        parcel.writeString(trueAnswer);
        parcel.writeList(answers);


    }
}
