package com.khalil.torya;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    private TextView result;
    ArrayList<Question> mWrongQuestions = new ArrayList<Question>();
    private RecyclerView mWrongQuestionsRecyclerView;
    private WrongQuestionAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Intent intent = getIntent();
        int count = 0;
        result = (TextView) findViewById(R.id.result);

        Bundle extras = getIntent().getExtras();


        if (extras != null) {
            ArrayList<String> trueAnswers = extras.getStringArrayList("trueAnswers");
            ArrayList<Question> questions = extras.getParcelableArrayList("questions");
            String [] results = extras.getStringArray("finalResult");

            try {
                assert trueAnswers != null;
                for(int i = 0; i<trueAnswers.size(); i++){
                    if((results != null ? results[i] : "NA").equals(trueAnswers.get(i))){

                        count++;
                    }else {
                        mWrongQuestions.add(questions.get(i));
                    }
                }
                if(count > 25){
                    result.setTextColor(this.getResources().getColor(R.color.colorPrimary));
                }else {
                    result.setTextColor(this.getResources().getColor(R.color.colorAccent));
                }
                result.setText(String.valueOf(count));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        mWrongQuestionsRecyclerView = (RecyclerView) findViewById(R.id.wrong_questions);

        mAdapter = new WrongQuestionAdapter(mWrongQuestions,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mWrongQuestionsRecyclerView.setLayoutManager(mLayoutManager);
        //mWrongQuestionsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mWrongQuestionsRecyclerView.setAdapter(mAdapter);


    }
}
