package com.khalil.torya;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class WrongQuestionAdapter extends
        RecyclerView.Adapter<WrongQuestionAdapter.ViewHolder> {


    private ArrayList<Question> questions;
    private Context context;



    public WrongQuestionAdapter(ArrayList<Question> questions
            , Context ctx) {
        this.questions = questions;
        context = ctx;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wrong_answer_item, parent, false);

        WrongQuestionAdapter.ViewHolder viewHolder =
                new WrongQuestionAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String questionTitle = questions.get(position).getTitle();
        String imagePath = questions.get(position).getImagePath();
        holder.questionTitle.setText(questionTitle);
        Glide.with(context).load(imagePath).into(holder.questionSign);



        for(int i=0;i<questions.get(position).getAnswers().size();i++){
            if(questions.get(position).getAnswers().get(i).getAnswerTag().equals(questions.get(position).getTrueAnswer())){
                String trueAnswer = questions.get(position).getAnswers().get(i).getAnswerText();
                if(trueAnswer.contains("https")){
                    holder.questionTrueAnswer.setVisibility(View.GONE);
                    holder.questionTrueAnswerImage.setVisibility(View.VISIBLE);
                    holder.questionTrueAnswerImage.setEnabled(true);
                    Glide.with(context).load(trueAnswer).into(holder.questionTrueAnswerImage);
                }else{

                    holder.questionTrueAnswerImage.setVisibility(View.GONE);
                    holder.questionTrueAnswer.setVisibility(View.VISIBLE);
                    holder.questionTrueAnswer.setText(trueAnswer);
                }

            }

        }


    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView questionTitle;
        public ImageView questionSign;
        public TextView questionTrueAnswer;
        public ImageView questionTrueAnswerImage;
        public ViewHolder(View view) {
            super(view);
            questionTitle = (TextView) view.findViewById(R.id.question_title);
            questionSign = (ImageView)view.findViewById(R.id.question_sign);
            questionTrueAnswer = (TextView) view.findViewById(R.id.true_answer);
            questionTrueAnswerImage = (ImageView) view.findViewById(R.id.true_answer_image);

        }
    }
}
